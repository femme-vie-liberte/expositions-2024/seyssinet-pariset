.. index::
   ! Inauguration de l'exposition sur l'histoire du féminisme iranien

.. _inauguration_2024_03_14:

==================================================================================================================================
Mardi 14 mars 2023 **Inauguration de l'exposition sur l'histoire du féminisme iranien** à l'Hôtel de ville de Seyssinet-Pariset
==================================================================================================================================

- https://www.ville-seyssinet-pariset.fr/actualites/le-8-mars-et-apres
- https://www.youtube.com/@iranluttes/videos


.. _zoya_2024_03_14:

Discours de Zoya Daneshrad, membre d'Iran Solidarités
========================================================

.. youtube:: avrzZTjjKbo


.. _goli_2024_03_14:

Discours de Goli, membre de la LDH Iran
==============================================

.. youtube:: E6lWYBWOzIA


.. _mecreant_2024_03_14:

Discours de Déborah Mécréant, élu en charge de l’égalité des chances
======================================================================

.. youtube:: ZQ_uyVEBibw


.. _lissy_2024_03_14:

Discours du maire de Seyssinet-Pariset Guillaume Lissy
========================================================

.. youtube:: yLV9TPMtiMM


.. _lemariey_2024_03_14:

Discours de Corine Lemariey déléguée à la lutte contre les discriminations
==========================================================================================

.. youtube:: YEU4JVC2Q6g

