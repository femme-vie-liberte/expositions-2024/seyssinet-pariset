.. index::
   pair: Exposition "L’histoire du féminisme iranien; Le Travailleur Alpin (2024-03-14)

.. _ta_2024_03_17:

=====================================================================================================================================
2024-03-17 **Compte rendu de l'exposition "L’histoire du féminisme iranien (Femme Vie Liberté)** par le Travailleur Alpin |TA|
=====================================================================================================================================

- https://travailleur-alpin.fr/2024/03/17/seyssinet-pariset-exposition-lhistoire-du-feminisme-iranien/
- https://travailleur-alpin.fr/tag/iran/
- https://www.ville-seyssinet-pariset.fr/actualites/le-8-mars-et-apres

Dans le cadre d’une série d’événements sous le titre `"le 8 mars et après" <https://www.ville-seyssinet-pariset.fr/actualites/le-8-mars-et-apres>`_
consacrés au féminisme, la ville de Seyssinet-Pariset inaugurait le 14 mars 2024,
en mairie l’exposition "l’histoire du féminisme iranien", prêtée par la ville de Grenoble.

Déborah Mécréant
==================

Il revenait à :ref:`Déborah Mécréant <mecreant_2024_03_14>`, élu en charge
de l’égalité des chances d’ouvrir la cérémonie en présence de plusieurs
dizaines de personnes (NDLR, en fait ce sont Zoya et Goli qui ont parlé en premier)

Citation
    "Cette exposition qui sera présente jusqu’à fin mars :ref:`dans le hall de l’hôtel <exposition_halle>`
    de ville puis en avril à l’Arche, met en lumière le combat des féministes
    iraniennes pour leur vie et leur liberté, en mettant en valeur trois périodes
    clés et des figures marquantes de l’histoire des mouvements féministes…

    En 2022, l’Iran et le monde ont été marqués par le meurtre de :ref:`Mahsa Jina Amini <iran_luttes:mahsa_jina_amini>`,
    qui a poussé la population à se soulever contre le régime totalitaire en place,
    malgré des exécutions nombreuses et injustes en représailles à ce soulèvement.

    **Nous souhaitons rendre hommage à ces femmes qui se sont battues et poursuivent
    le combat pour faire entendre leur voix et faire reconnaître leurs droits**."

Guillaume Lissy
==================

:ref:`Guillaume Lissy <lissy_2024_03_14>`, maire, a explicité son soutien à cette initiative pour le
droit des femmes et remercié celles et ceux qui l’ont mise en œuvre.

Citation
    "Nous souhaitons que le :ref:`hall d’entrée <exposition_halle>` de la mairie soit un lieu d’exposition
    pour transmettre des valeurs d’éducation populaire, pour éclairer les consciences,
    notamment sur le combat des femmes."

Corine Lemariey
=================

Corine Lemariey, conseillère communautaire ,
déléguée à la lutte contre les discriminations est :ref:`intervenue <lemariey_2024_03_14>`.

Roya Daneshrad et Goli
============================

- :ref:`zoya_2024_03_14`
- :ref:`goli_2024_03_14`

Deux représentantes du collectif Iran Solidarités et de la ligue pour la
défense des droits de l’homme en Iran ont pris la parole.


Le groupe de musique, la chanson Barâyé et son auteur Shervin Hajipour
========================================================================

- https://en.wikipedia.org/wiki/Shervin_Hajipour

Un groupe a ensuite lu quelques poèmes iraniens et chanté de très belles chansons
iraniennes dont la chanson :ref:`Barâyé <iran_luttes:baraye>`, qui est devenue l’hymne du mouvement
"femme, vie, liberté".

L’auteur de cette chanson, Shervin Hajipour, a été arrêté le 22 septembre 2022
pour ce texte qui reprend toutes les revendications du peuple iranien.

Cette chanson écouté 40 millions de fois en deux jours puis reprise dans le
monde entier.

Son auteur sera emprisonné, libéré et se retrouve aujourd’hui sous les verrous.

.. note:: Le 1er mars 2024, Shervin Hajipour est condamné par la justice iranienne,
   à quatre ans de prison et condamné à écrire de la musique anti-américaine

   On 1 March 2024, Hajipour was sentenced to serve up to four years in jail,
   with eight months for creating propaganda against the government and three
   years for "encouraging and provoking the public to riot to disrupt national
   security."[38]
   Along with the jail sentence, Hajipour was ordered to write anti-America-Aggresion
   music regarding their crimes against humanity and human rights violations


   Voir https://fr.wikipedia.org/wiki/Shervin_Hajipour

