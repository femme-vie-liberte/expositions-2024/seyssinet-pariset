.. index::
   ! Exposition halle d'entrée de l'Hôtel de ville


.. _exposition_halle:

===================================================================================
**L'exposition dans le Hall d'entrée de l'Hôtel de Ville de Seyssinet Pariset**
===================================================================================

.. figure:: ../images/hotel_de_ville.jpg

.. figure:: images/20240314_184630_800.jpg

Partie droite en entrant : historique
==========================================

- Voir :ref:`expo_grenoble:salle_1_grenoble`


.. figure:: images/20240314_171833_800.jpg

Les 3 périodes du féminisme iranien
---------------------------------------

- :ref:`expo_grenoble:trois_periodes`

L'éveil des femmes
++++++++++++++++++++++

- :ref:`expo_grenoble:eveil_des_femmes`

.. figure:: images/20240314_184732_800.jpg

L'éducation des femmes
++++++++++++++++++++++

- :ref:`expo_grenoble:education_des_femmes`


.. figure:: images/20240314_184726_800.jpg

Le journalisme (1940-1920)
++++++++++++++++++++++++++++++++

- :ref:`expo_grenoble:journalisme`

.. figure:: images/20240314_184711_800.jpg


1925-1941
------------

.. figure:: images/20240314_185901_800.jpg


Qamar-ol-Moluk Vaziri, (1905-1959) chanteuse iranienne célèbre et Shirin Ebadi
------------------------------------------------------------------------------------

- :ref:`expo_grenoble:qamar_vaziri`
- :ref:`expo_grenoble:shirin_ebadi`

.. figure:: images/20240314_184704_800.jpg


|ForoughFarrokhzad|  Forough Farrokhzad
-----------------------------------------------

- :ref:`expo_grenoble:forough_farrokhzad`

.. figure:: images/20240314_184718_800.jpg



Partie gauche en entrant : la période récente
==================================================

.. figure:: images/20240314_172024_800.jpg



Les dessins de Bahareh Akrami
-------------------------------

- :ref:`akrami_iran:dessins_bahareh_akrami`

.. figure:: images/20240314_184546_800.jpg

.. figure:: images/20240314_184500_800.jpg


Femme Vie Liberté
----------------------


.. figure:: images/20240314_184537_800.jpg

Jina Mahsa amini
----------------------

- :ref:`iran_luttes:mahsa_jina_amini`

La révolution Mahsa Jina Amini
-----------------------------------------------

.. figure:: images/20240314_184434_800.jpg

Cérémonie de la mort de Jina Mahsa Amini
-----------------------------------------------

.. figure:: images/20240314_184552_800.jpg


Shervin Hajipour
----------------------

- :ref:`iran_luttes:shervin_hajipour`

.. figure:: images/20240314_184603_800.jpg

Les personnes assassinées
----------------------------------

- :ref:`iran_luttes:assassinats_luttes_iran`

.. figure:: images/20240314_184612_800.jpg


Kian pirfalak
---------------

- :ref:`iran_luttes:kian_pirfalak`

.. figure:: images/20240314_184443_800.jpg
