# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
# sys.path.insert(0, os.path.abspath("./src"))

project = "Femme Vie Liberté 2024, Seyssinet-Pariset"
html_title = project

author = f"Human people"
html_logo = "images/logo_jina_amini_small.png"
html_favicon = "images/logo_jina_amini_small.png"
release = "0.1.0"
now = datetime.now(tz=ZoneInfo("Iran"))
# version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
version = f"2024-03-08 => 2024-04-29"
today = version

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx_copybutton",
    "sphinxcontrib.youtube",
]
autosummary_generate = True
autoclass_content = "class"

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".venv"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "culture_perse": ("https://iran.frama.io/culture-perse/", None),
    "iran_luttes": ("https://iran.frama.io/luttes/", None),
    "iran_2023": ("https://iran.frama.io/luttes-2023/", None),
    "akrami_iran": ("https://bahareh-akrami.frama.io/iran/", None),
    "tuto_fediverse": ("https://gdevops.frama.io/web/fediverse/", None),
    "rojava_luttes": ("https://rojava.frama.io/luttes/", None),
    "kurdistan_luttes": ("https://kurdistan.frama.io/luttes/", None),
    "chinsky": ("https://judaism.gitlab.io/floriane_chinsky/", None),
    "expo_grenoble": ("https://femme-vie-liberte.frama.io/expositions-2023/grenoble/", None),
}
extensions.append("sphinx.ext.todo")
todo_include_todos = True


# material theme options (see theme.conf for more information)
# https://gitlab.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

# material theme options (see theme.conf for more information)
html_theme_options = {
    "base_url": "https://femme-vie-liberte.frama.io/expositions-2024/seyssinet-pariset",
    "repo_url": "https://framagit.org/femme-vie-liberte/expositions-2024/seyssinet-pariset",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "green",
    "color_accent": "cyan",
    "theme_color": "#2196f3",
    "nav_title": f"{project} ({today})",
    "master_doc": False,
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://iran.frama.io/luttes/",
            "internal": False,
            "title": "Iran luttes",
        },
        {
            "href": "https://femme-vie-liberte.frama.io/expositions-2023/grenoble/",
            "internal": False,
            "title": "Exposition 2023 à Grenoble",
        },
        {
            "href": "https://bahareh-akrami.frama.io/iran/",
            "internal": False,
            "title": "Dessins Bahareh Akrami",
        },
        {
            "href": "http://iran.frama.io/linkertree",
            "internal": False,
            "title": "Liens iranluttes",
        },
    ],
    "heroes": {
        "index": "Femme Vie Liberté 2024, Seyssinet-Pariset",
    },
    "table_classes": ["plain"],
}
# https://github.com/sphinx-contrib/yasfb
# https://github.com/sphinx-contrib/yasfb
extensions.append("yasfb")
feed_base_url = html_theme_options["base_url"]
feed_author = "Scribe"
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")
# https://sphinx-tags.readthedocs.io/en/latest/quickstart.html#installation
extensions.append("sphinx_tags")
tags_create_tags = True
# Whether to display tags using sphinx-design badges.
tags_create_badges = True

language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True

copyright = f"-2500-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

rst_prolog = """
.. |JinaAmini| image:: /images/mahsa_jina_amini_avatar.png
.. |TA| image:: /images/ta_avatar.png
.. |ForoughFarrokhzad| image:: /images/forough_farrokhzad_avatar.png
.. |BaharehAkrami| image:: /images/bahareh_akrami_avatar.png
.. |FluxWeb| image:: /images/rss_avatar.webp
"""
