
.. ☠️
.. ☣️
.. 🪧
.. 🇺🇳
.. 🇫🇷
.. 🇧🇪
.. 🇨🇭
.. 🇩🇪
.. 🌍 ♀️
.. 🇮🇷



|FluxWeb| `RSS <https://femme-vie-liberte.frama.io/expositions-2024/seyssinet-pariset/rss.xml>`_

.. _exposition_iran:
.. _iran_exposition:
.. _femme_vie_liberte:

============================================================================================================================================================================================
|ForoughFarrokhzad| **Femme Vie Liberté, Exposition sur l’histoire du féminisme iranien** du vendredi 8 mars au mardi 29 mars 2024 à l'hôtel de ville de Seyssinet-Pariset |JinaAmini|
============================================================================================================================================================================================

- http://iran.frama.io/linkertree
- https://fr.wikipedia.org/wiki/Seyssinet-Pariset
- https://www.ville-seyssinet-pariset.fr/nouveau-conseil-municipal
- https://www.ville-seyssinet-pariset.fr/


.. figure:: images/le_8_mars_et_apres.webp
   :align: center
   :width: 500

   https://www.ville-seyssinet-pariset.fr/actualites/le-8-mars-et-apres


L'exposition **Femme Vie Liberté** se donne comme objectif de mettre en
lumière la lutte des femmes iraniennes.

`Du 8 au 29 mars 2024 Expo > Femme Vie Liberté <https://www.ville-seyssinet-pariset.fr/actualites/le-8-mars-et-apres>`_
Hall de l’Hôtel de Ville, puis à l’Arche en avril
Vernissage de l’expo : Jeudi 14 mars 17h30, Hall de l’Hôtel de Ville

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.694740116596223%2C45.179838058360794%2C5.69828063249588%2C45.181509364753744&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.18067/5.69651">Afficher une carte plus grande</a></small>


::

    Hôtel de ville de Seyssinet-Pariset
    Place André-Balme
    38170 Seyssinet-Pariset
    France

.. figure:: images/hotel_de_ville_loin.jpg


.. figure:: images/hotel_de_ville.jpg


Le Collectif Iran Solidarités et la Ligue pour la défense des droits de
l'Homme en Iran vous proposent cette exposition consacrée à L'Iran
**Femme, Vie, Liberté**, trois mots qui secouent le monde depuis 18 mois,
trois mots qui sont devenus l'emblème d'une révolution en marche en Iran.

Depuis la mort de Mahsa Jina Amini, ce slogan est scandé dans toutes les rues en
Iran pour réclamer les droits fondamentaux des femmes et de toute La population

Réprimé dans le sang depuis 1979 (45 ans), le peuple iranien est déterminé
à reprendre son destin en main.


.. toctree::
   :maxdepth: 6

   halle/halle
   03/03
